extends Area3D
class_name HitBox
## Dummy class for keeping the HitBox system organized

static var RECEIVER = HitBoxReceiver
static var HITTER = HitBoxHitter
static var HITTER_HITSCAN_SHAPE = HitBoxHitterHitscanShape

const DEFAULT_LAYER:int = 2147483648
